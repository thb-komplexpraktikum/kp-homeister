package de.thb.kp.homeister.yuckychokky.ui.console;

import java.util.stream.Collectors;

import de.thb.kp.homeister.yuckychokky.core.Board;
import de.thb.kp.homeister.yuckychokky.core.Direction;
import de.thb.kp.homeister.yuckychokky.core.Player;
import de.thb.kp.homeister.yuckychokky.ui.TurnUI;

public class TurnUIConsoleHuman implements TurnUI {

  @Override
  public void displayTurnStart(Player player) {
    System.out.println(String.format("It's your turn %s", player.getName()));
  }

  @Override
  public void displayTurnEnd(Player player, Direction direction, int count) {
    //Do nothing on purpose
  }

  @Override
  public Direction getDirection(Board board) {
    String message = String.format("Enter the direction in which you want to break (%s):", Direction.getAllShortForms().stream().collect(Collectors.joining(", ")));
    Direction direction = Console.readDirection(message);
    while(!board.isValidRemoveDirection(direction)) {
      System.out.println("You can't break in this direction.");
      direction = Console.readDirection(message);
    }
    return direction;
  }

  @Override
  public int getCount(Board board, Direction direction) {
    String message = "Enter the amount you want to break: ";
    int count = Console.readInt(message);
    int maxRemove = board.maxRemove(direction);
    while(count == 0 || maxRemove < count) {
      if (count == 0) {
        System.out.println("You can't skip your turn.");
      } else {
        System.out.println(String.format("You can only break up to %d in this direction.", maxRemove));
      }
      count = Console.readInt(message);
    }
    return count;
  }
}
