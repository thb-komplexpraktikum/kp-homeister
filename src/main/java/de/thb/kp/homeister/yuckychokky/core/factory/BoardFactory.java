package de.thb.kp.homeister.yuckychokky.core.factory;

import de.thb.kp.homeister.yuckychokky.core.Board;
import de.thb.kp.homeister.yuckychokky.core.BoardStandard;
import de.thb.kp.homeister.yuckychokky.ui.BoardUI;
import de.thb.kp.homeister.yuckychokky.ui.factory.BoardUIFactory;

public class BoardFactory {
  public static Board createBoard() {
    BoardUI boardUI = BoardUIFactory.createBoardUI();
    return new BoardStandard(boardUI);
  }
}
