package de.thb.kp.homeister.yuckychokky.core;

import de.thb.kp.homeister.yuckychokky.ui.BoardUI;

public interface Board {
  public BoardUI getBoardUI();
  public void setHeight(int height);
  public int getHeight();
  public void setWidth(int width);
  public int getWidth();
  public void setBoardPiece(BoardPiece piece,int x,int y);
  public void removePieces(Direction direction, int count);
  public void create();
  public void updateUI();
  public boolean isPlayable();
  public boolean isValidRemoveDirection(Direction direction);
  public int maxRemove(Direction direction);
}
