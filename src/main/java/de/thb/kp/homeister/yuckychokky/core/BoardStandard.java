package de.thb.kp.homeister.yuckychokky.core;

import java.util.Arrays;
import java.util.stream.Stream;

import de.thb.kp.homeister.yuckychokky.ui.BoardUI;

public class BoardStandard implements Board {

  private int height;
  private int width;
  private BoardPiece[][] board;
  private BoardUI boardUI;

  public BoardStandard(BoardUI boardUI) {
    this.boardUI = boardUI;
  }

  @Override
  public void setHeight(int height) {
    this.height = height;
  }

  @Override
  public int getHeight() {
    return this.height;
  }

  @Override
  public void setWidth(int width) {
    this.width = width;
  }

  @Override
  public int getWidth() {
    return this.width;
  }

  @Override
  public BoardUI getBoardUI() {
    return this.boardUI;
  }

  @Override
  public void setBoardPiece(BoardPiece piece, int x, int y) {
    this.board[x][y] = piece;
  }

  @Override
  public void create() {
    this.board = new BoardPiece[this.height][this.width];
    for(BoardPiece[] row: this.board) {
      Arrays.fill(row, BoardPiece.CHOCOLATE);
    }
    this.board[0][0] = BoardPiece.SOAP;
  }

  @Override
  public void updateUI() {
    this.boardUI.display(this.board);
  }

  @Override
  public void removePieces(Direction direction, int count) {
    switch(direction) {
      case HORIZONTAL: 
        for(int i = this.board.length - 1; i > 0; i--) {
          if(count == 0) break;
          if(!(board[i][0] == BoardPiece.CHOCOLATE)) {
            continue;
          }
          Arrays.fill(board[i], BoardPiece.NOTHING);
          count--;
        }
        break;
      case VERTICAL: 
        for(BoardPiece[] row: this.board) {
          int tmpCount = count;
          for(int j = row.length - 1; j > 0; j--) {
            if(tmpCount == 0) break;
            if(!(row[j] == BoardPiece.CHOCOLATE)) {
              continue;
            }
            row[j] = BoardPiece.NOTHING;
            tmpCount--;
          }
        }
        break;
      default: throw new IllegalArgumentException(String.format("Unknown Direction %s", direction.name()));
    }
  }

  @Override
  public boolean isPlayable() {
    Stream<BoardPiece> pieces = Arrays.stream(this.board).flatMap(Arrays::stream);
    return pieces.anyMatch(piece -> piece == BoardPiece.CHOCOLATE);
  }

  @Override
  public boolean isValidRemoveDirection(Direction direction) {
    return this.maxRemove(direction) > 0;
  }

  @Override
  public int maxRemove(Direction direction) {
    Stream<BoardPiece> firstRow = Arrays.stream(this.board[0]);
    Stream<BoardPiece> firstColumn = Arrays.stream(this.board).map(row -> row[0]);
    int count = 0;
    switch(direction) {
      case HORIZONTAL: count = (int)firstColumn.filter(piece -> piece == BoardPiece.CHOCOLATE).count(); break;
      case VERTICAL: count = (int)firstRow.filter(piece -> piece == BoardPiece.CHOCOLATE).count(); break;
      default: throw new IllegalArgumentException(String.format("Unknown Direction %s", direction.name()));
    }
    return count;
  }
}
