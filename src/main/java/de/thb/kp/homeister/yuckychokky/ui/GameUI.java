package de.thb.kp.homeister.yuckychokky.ui;

import de.thb.kp.homeister.yuckychokky.core.Player;

public interface GameUI {
  public void initialize();
  public int getBoardHeight();
  public int getBoardWidth();
  public int getHumanPlayerCount(int min, int max);
  public void announceStartingPlayer(Player player);
  public void announceWinner(Player player);
}
