package de.thb.kp.homeister.yuckychokky.core;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Direction {
  HORIZONTAL("h"),
  VERTICAL("v");

  private String shortForm;

  private Direction(String shortForm) {
    this.shortForm = shortForm;
  }

  public String getShortForm() {
    return this.shortForm;
  }

  public static List<String> getAllShortForms() {
    return Stream.of(Direction.values())
      .map((direction) -> direction.getShortForm())
      .collect(Collectors.toList());
  }

  public static Direction getFromShortForm(String shortForm) {
    return Stream.of(Direction.values())
      .filter((direction) -> direction.getShortForm().equals(shortForm))
      .findFirst().orElse(null);
  }
}
