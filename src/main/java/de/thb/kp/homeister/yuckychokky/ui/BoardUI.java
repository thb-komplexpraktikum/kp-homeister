package de.thb.kp.homeister.yuckychokky.ui;

import de.thb.kp.homeister.yuckychokky.core.BoardPiece;

public interface BoardUI {
  public void display(BoardPiece[][] board);
}
