package de.thb.kp.homeister.yuckychokky.core;

import de.thb.kp.homeister.yuckychokky.ui.TurnUI;

public class HumanPlayer implements Player {

  private TurnUI turnUI;
  private String name;

  public HumanPlayer(TurnUI turnUI) {
    this.turnUI = turnUI;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public TurnUI getPlayerTurnUI() {
    return turnUI;
  }

  @Override
  public void executeTurn(Board board) {
    board.updateUI();
    this.turnUI.displayTurnStart(this);
    Direction direction = this.turnUI.getDirection(board);
    int count = this.turnUI.getCount(board, direction);
    board.removePieces(direction, count);
    this.turnUI.displayTurnEnd(this, direction, count);
  }
}
