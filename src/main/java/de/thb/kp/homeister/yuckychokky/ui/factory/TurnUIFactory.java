package de.thb.kp.homeister.yuckychokky.ui.factory;

import de.thb.kp.homeister.yuckychokky.ui.TurnUI;
import de.thb.kp.homeister.yuckychokky.ui.console.TurnUIConsoleAI;
import de.thb.kp.homeister.yuckychokky.ui.console.TurnUIConsoleHuman;

public class TurnUIFactory {
  public static TurnUI createHumanTurnUI() {
    return new TurnUIConsoleHuman();
  }

  public static TurnUI createAITurnUI() {
    return new TurnUIConsoleAI();
  }
}
