package de.thb.kp.homeister.yuckychokky.ai;

import java.util.HashMap;
import java.util.Map;

import de.thb.kp.homeister.yuckychokky.core.Board;
import de.thb.kp.homeister.yuckychokky.core.Direction;
import de.thb.kp.homeister.yuckychokky.core.Player;
import de.thb.kp.homeister.yuckychokky.ui.TurnUI;

public class AIPlayer implements Player {

  private TurnUI turnUI;
  private String name;

  public AIPlayer(TurnUI turnUI) {
    this.turnUI = turnUI;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public TurnUI getPlayerTurnUI() {
    return turnUI;
  }

  @Override
  public void executeTurn(Board board) {
    board.updateUI();
    this.turnUI.displayTurnStart(this);
    Map.Entry<Direction, Integer> direction = this.getBestTurn(board);
    board.removePieces(direction.getKey(), direction.getValue());
    this.turnUI.displayTurnEnd(this, direction.getKey(), direction.getValue());
  }

  private Map.Entry<Direction, Integer> getBestTurn(Board board) {
    Map<Direction, Integer> maxRemoves = new HashMap<Direction, Integer>();
    for(Direction direction : Direction.values()) {
      int count = board.maxRemove(direction);
      maxRemoves.put(direction, count);
    }

    Map.Entry<Direction, Integer> maxDirection = maxRemoves
      .entrySet().stream()
      .max((entry1, entry2) -> entry1.getValue().compareTo(entry2.getValue()))
      .get();

    Map.Entry<Direction, Integer> minDirection = maxRemoves
      .entrySet().stream()
      .min((entry1, entry2) -> entry1.getValue().compareTo(entry2.getValue()))
      .get();

    int count = maxDirection.getValue() - minDirection.getValue();
    count = count == 0 ? 1 : count;
    maxDirection.setValue(count);

    return maxDirection;
  }
}
