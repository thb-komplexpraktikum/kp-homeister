package de.thb.kp.homeister.yuckychokky.core.factory;

import de.thb.kp.homeister.yuckychokky.core.Board;
import de.thb.kp.homeister.yuckychokky.core.Game;
import de.thb.kp.homeister.yuckychokky.core.GameTwoPlayer;
import de.thb.kp.homeister.yuckychokky.ui.GameUI;
import de.thb.kp.homeister.yuckychokky.ui.factory.GameUIFactory;

public class GameFactory {
  public static Game createGame() {
    GameUI gameUI = GameUIFactory.createGameUI();
    Board board = BoardFactory.createBoard();
    Game game = new GameTwoPlayer(gameUI, board);
    return game;
  }
}
