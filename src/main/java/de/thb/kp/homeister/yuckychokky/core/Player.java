package de.thb.kp.homeister.yuckychokky.core;

import de.thb.kp.homeister.yuckychokky.ui.TurnUI;

public interface Player {
  public TurnUI getPlayerTurnUI();
  public void setName(String name);
  public String getName();
  public void executeTurn(Board board);
}
