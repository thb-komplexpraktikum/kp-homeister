package de.thb.kp.homeister.yuckychokky.ui.factory;

import de.thb.kp.homeister.yuckychokky.ui.GameUI;
import de.thb.kp.homeister.yuckychokky.ui.console.GameUIConsole;

public class GameUIFactory {
  public static GameUI createGameUI() {
    return new GameUIConsole();
  }
}
