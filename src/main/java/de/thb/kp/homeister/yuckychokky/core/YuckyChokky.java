package de.thb.kp.homeister.yuckychokky.core;

import de.thb.kp.homeister.yuckychokky.core.factory.GameFactory;

public class YuckyChokky {

  public static void main(String[] args) {
    Game game = GameFactory.createGame();
    game.setup();
    game.start();
    game.conclude();
  }
}
