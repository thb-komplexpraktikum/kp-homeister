package de.thb.kp.homeister.yuckychokky.core.factory;

import de.thb.kp.homeister.yuckychokky.ai.AIPlayer;
import de.thb.kp.homeister.yuckychokky.core.HumanPlayer;
import de.thb.kp.homeister.yuckychokky.core.Player;
import de.thb.kp.homeister.yuckychokky.ui.TurnUI;
import de.thb.kp.homeister.yuckychokky.ui.factory.TurnUIFactory;

public class PlayerFactory {
  public static Player createHumanPlayer() {
    TurnUI turnUI = TurnUIFactory.createHumanTurnUI();
    return new HumanPlayer(turnUI);
  }

  public static Player createAIPlayer() {
    TurnUI turnUI = TurnUIFactory.createAITurnUI();
    return new AIPlayer(turnUI);
  }
}
