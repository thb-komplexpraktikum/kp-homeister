package de.thb.kp.homeister.yuckychokky.ui.console;

import java.util.Scanner;
import java.util.stream.Collectors;

import de.thb.kp.homeister.yuckychokky.core.Direction;

public class Console {
  private static final Scanner in = new Scanner(System.in);

  public static int readInt(String message) {
    System.out.print(message);
    while(!Console.in.hasNextInt()) {
      System.out.println("Please enter an integer.");
      Console.in.next();
      System.out.print(message);
    }
    return Console.in.nextInt();
  }

  public static int readInt(String message, int min, int max) {
    int result = Console.readInt(message);
    while(result < min || result > max) {
      System.out.println(String.format("Enter a value between %d and %d.", min, max));
      result = Console.readInt(message);
    }
    return result;
  }

  public static Direction readDirection(String message) {
    System.out.print(message);
    String input = Console.in.next();
    Direction direction = Direction.getFromShortForm(input);
    while(direction == null) {
      System.out.println(String.format("Enter a valid Direction (%s).",Direction.getAllShortForms().stream().collect(Collectors.joining(", "))));
      System.out.print(message);
      input = Console.in.next();
      direction = Direction.getFromShortForm(input);
    }
    return direction;
  }
}
