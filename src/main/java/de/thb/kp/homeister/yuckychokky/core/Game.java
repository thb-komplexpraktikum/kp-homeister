package de.thb.kp.homeister.yuckychokky.core;

import de.thb.kp.homeister.yuckychokky.ui.GameUI;

public interface Game {
  public Board getBoard();
  public GameUI getGameUI();

  public void setup();
  public void start();
  public void conclude();
}
