package de.thb.kp.homeister.yuckychokky.ui.console;

import de.thb.kp.homeister.yuckychokky.core.BoardPiece;
import de.thb.kp.homeister.yuckychokky.ui.BoardUI;

public class BoardUIConsole implements BoardUI {

  @Override
  public void display(BoardPiece[][] board) {
    for(BoardPiece[] row: board) {
      for(BoardPiece piece: row) {
        String pieceRepresentation;
        switch(piece) {
          case SOAP: pieceRepresentation = "*"; break;
          case CHOCOLATE: pieceRepresentation = "o"; break;
          case NOTHING: pieceRepresentation = " "; break;
          default: throw new IllegalArgumentException(String.format("Unknown BoardPiece %s", piece.name()));
        }
        System.out.print(String.format("%s ", pieceRepresentation));
      }
      System.out.println();
    }
  }

}
