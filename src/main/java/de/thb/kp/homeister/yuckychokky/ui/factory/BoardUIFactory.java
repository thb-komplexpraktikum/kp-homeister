package de.thb.kp.homeister.yuckychokky.ui.factory;

import de.thb.kp.homeister.yuckychokky.ui.BoardUI;
import de.thb.kp.homeister.yuckychokky.ui.console.BoardUIConsole;

public class BoardUIFactory {
  public static BoardUI createBoardUI() {
    return new BoardUIConsole();
  }
}
