package de.thb.kp.homeister.yuckychokky.ui.console;

import de.thb.kp.homeister.yuckychokky.core.Player;
import de.thb.kp.homeister.yuckychokky.ui.GameUI;

public class GameUIConsole implements GameUI {

  @Override
  public void initialize() {
    //Do nothing on purpose
  }

  @Override
  public int getBoardHeight() {
    return Console.readInt("Enter the height of the chocolate: ");
  }

  @Override
  public int getBoardWidth() {
    return Console.readInt("Enter the width of the chocolate: ");
  }

  @Override
  public int getHumanPlayerCount(int min, int max) {
    return Console.readInt(String.format("Enter the count of human players (%d-%d): ", min, max), min, max);
  }

  @Override
  public void announceStartingPlayer(Player player) {
    System.out.println(player.getName() + " you are first.");
  }

  @Override
  public void announceWinner(Player player) {
    System.out.println(player.getName() + " has won!");
  }
}
