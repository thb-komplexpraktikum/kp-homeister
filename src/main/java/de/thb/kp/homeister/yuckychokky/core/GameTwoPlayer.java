package de.thb.kp.homeister.yuckychokky.core;

import java.util.Random;

import de.thb.kp.homeister.yuckychokky.core.factory.PlayerFactory;
import de.thb.kp.homeister.yuckychokky.ui.GameUI;

public class GameTwoPlayer implements Game {

  private Board board;
  private GameUI gameUI;
  private Player[] players;
  private int currentPlayer;

  @Override
  public Board getBoard() {
    return this.board;
  }

  @Override
  public GameUI getGameUI() {
    return this.gameUI;
  }

  public GameTwoPlayer(GameUI gameUI, Board board) {
    this.gameUI = gameUI;
    this.board = board;
  }

  @Override
  public void setup() {
    int playersCount = 2;
    int height = this.gameUI.getBoardHeight();
    int width = this.gameUI.getBoardWidth();
    this.board.setHeight(height);
    this.board.setWidth(width);

    int humanPlayers = this.gameUI.getHumanPlayerCount(0, playersCount);

    this.players = new Player[playersCount];
    for(int i = 0; i < playersCount ;i++) {
      if(i < humanPlayers) {
        players[i] = PlayerFactory.createHumanPlayer();
      } else {
        players[i] = PlayerFactory.createAIPlayer();
      }
      players[i].setName(String.format("Player %d", i+1));
    }
  }

  @Override
  public void start() {
    this.board.create();
    this.currentPlayer = (new Random()).nextInt(this.players.length);
    this.gameUI.announceStartingPlayer(this.players[currentPlayer]);

    while(this.board.isPlayable()) {
      this.players[currentPlayer].executeTurn(this.board);
      this.currentPlayer = this.getNextPlayer();
    }
  }

  @Override
  public void conclude() {
    int winner = this.getPreviousPlayer();
    this.gameUI.announceWinner(this.players[winner]);
  }

  private int getPreviousPlayer() {
    if(this.currentPlayer == 0) {
      return this.players.length - 1;
    } else {
      return this.currentPlayer - 1;
    }
  }

  private int getNextPlayer() {
    if(this.currentPlayer == this.players.length - 1) {
      return 0;
    } else {
      return this.currentPlayer + 1;
    }
  }
}
