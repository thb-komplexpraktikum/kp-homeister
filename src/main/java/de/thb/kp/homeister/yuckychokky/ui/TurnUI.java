package de.thb.kp.homeister.yuckychokky.ui;

import de.thb.kp.homeister.yuckychokky.core.Board;
import de.thb.kp.homeister.yuckychokky.core.Direction;
import de.thb.kp.homeister.yuckychokky.core.Player;

public interface TurnUI {
  public void displayTurnStart(Player player);
  public void displayTurnEnd(Player player, Direction direction, int count);
  public Direction getDirection(Board board);
  public int getCount(Board board, Direction direction);
}
