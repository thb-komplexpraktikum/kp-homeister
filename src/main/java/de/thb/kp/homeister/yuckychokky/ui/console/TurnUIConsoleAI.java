package de.thb.kp.homeister.yuckychokky.ui.console;

import de.thb.kp.homeister.yuckychokky.core.Board;
import de.thb.kp.homeister.yuckychokky.core.Direction;
import de.thb.kp.homeister.yuckychokky.core.Player;
import de.thb.kp.homeister.yuckychokky.ui.TurnUI;

public class TurnUIConsoleAI implements TurnUI {

  @Override
  public void displayTurnStart(Player player) {
    System.out.println(String.format("It's the turn of the AI %s.", player.getName()));
  }

  @Override
  public void displayTurnEnd(Player player, Direction direction, int count) {
    String pieces = (count == 1) ? "piece" : "pieces";
    System.out.println(String.format("The AI %s broke off %d %s %sly.", player.getName(), count, pieces, direction.name().toLowerCase()));
  }

  @Override
  public Direction getDirection(Board board) {
    throw new UnsupportedOperationException("Nothing to implement.");
  }

  @Override
  public int getCount(Board board, Direction direction) {
    throw new UnsupportedOperationException("Nothing to implement.");
  }
}
